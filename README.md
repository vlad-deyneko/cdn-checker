# CDNChecker #

This project is a part of the research made for ISAT 2017 conference. It's main purpose to detect CDN and to take out additional web page info for specified hosts. Project is open-source and you can modify it and use as you want for your own purposes without my permission. If you have any questions then feel free to contact me.



### How do I get set up? ###

* Install Visual Studio 2012/2015/2017
* Build the project with all dependencies
* Run the project on your local machine to test it
* You can build the release version for your own needs

### Contribution guidelines ###

If you want to help me with this project or have interesting ideas, you an contact me at:
[v.deineko@outlook.com](mailto:v.deineko@outlook.com)

### Contact ###

Project author: Vladyslav Deyneko ([v.deineko@outlook.com](v.deineko@outlook.com))