﻿
namespace CDNChecker.Models
{
    class PageSpeedReport
    {
        public string Id { get; set; }
        public string ResponseCode { get; set; }
        public string Title { get; set; }
        public RuleGroup RuleGroups { get; set; }
        public PageStatistics PageStats { get; set; }
    }

    public class RuleGroup
    {
        public Rule Speed { get; set; }
        public Rule Usability { get; set; }
    }

    public class Rule
    {
        public int Score { get; set; }
    }
}
