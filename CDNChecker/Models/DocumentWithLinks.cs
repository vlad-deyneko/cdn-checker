﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace CDNChecker.Models
{
    class DocumentWithLinks
    {
        private List<Script> scripts;
        private List<Style> styles;
        private List<Image> images;
        private List<Url> references;
        private HtmlDocument doc;

        /// <summary>
        /// Gets a list of scripts as they are declared in the HTML document.
        /// </summary>
        public List<Script> Scripts => scripts;

        /// <summary>
        /// Gets a list of reference links to other HTML documents, as they are declared in the HTML document.
        /// </summary>
        public List<Url> References => references;

        /// <summary>
        /// Gets a list of styles as they are declared in the HTML document.
        /// </summary>
        public List<Style> Styles => styles;

        /// <summary>
        /// Gets a list of images as they are declared in the HTML document.
        /// </summary>
        public List<Image> Images => images;

        /// <summary>
        /// Creates an instance of a DocumentWithLinkedFiles.
        /// </summary>
        /// <param name="document">The input HTML document. May not be null.</param>
        public DocumentWithLinks(HtmlDocument document)
        {
            if (document != null)
            {
                doc = document;
                GetScripts();
                GetImages();
                GetStyles();
                GetReferences();
            }
        }

        private void GetScripts()
        {
            scripts = new List<Script>();
            HtmlNodeCollection scriptNodes = doc.DocumentNode.SelectNodes("//script");
            if (scriptNodes != null)
            {
                foreach (HtmlNode script in scriptNodes)
                {
                    scripts.Add(new Script
                    {
                        Src = script.Attributes["src"] != null ? script.Attributes["src"].Value : ""
                        //Type = script.Attributes["type"].Value
                    });
                }
            }
        }

        private void GetImages()
        {
            images = new List<Image>();
            HtmlNodeCollection imageNodes = doc.DocumentNode.SelectNodes("//img[@src]");
            if (imageNodes != null)
            {
                foreach (HtmlNode image in imageNodes)
                {
                    images.Add(new Image
                    {
                        Alt = image.Attributes["alt"]?.Value,
                        Src = image.Attributes["src"].Value
                    });
                }
            }
        }

        private void GetStyles()
        {
            styles = new List<Style>();
            HtmlNodeCollection styleNodes = doc.DocumentNode.SelectNodes("//link[@rel and @href]");
            if (styleNodes != null)
            {
                foreach (HtmlNode style in styleNodes.Where(c => c.Attributes["rel"].Value == "stylesheet"))
                {
                    styles.Add(new Style
                    {
                        Href = style.Attributes["href"].Value,
                        Rel = style.Attributes["rel"].Value
                    });
                }
            }
        }

        private void GetReferences()
        {
            references = new List<Url>();
            HtmlNodeCollection hrefs = doc.DocumentNode.SelectNodes("//a[@href]");
            if (hrefs != null)
            {
                foreach (HtmlNode href in hrefs)
                {
                    references.Add(new Url
                    {
                        Href = href.Attributes["href"].Value
                    });
                }
            }
        }
    }
}
