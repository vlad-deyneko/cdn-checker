﻿
namespace CDNChecker.Models
{
    class HostInfo
    {
        public string As { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Query { get; set; }
        public string Timezone { get; set; }
        public string Status { get; set; }
    }
}
