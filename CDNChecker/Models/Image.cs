﻿
namespace CDNChecker.Models
{
    class Image
    {
        public string Src { get; set; }
        public string Alt { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
