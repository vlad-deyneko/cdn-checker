﻿namespace CDNChecker
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.AnalyzeButton = new System.Windows.Forms.Button();
            this.urlInput = new System.Windows.Forms.TextBox();
            this.DetailsListView = new System.Windows.Forms.ListView();
            this.type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.elementUrl = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HistoryListView = new System.Windows.Forms.ListView();
            this.ItemNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ItemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ItemCreationDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.RequestUriLabel = new System.Windows.Forms.Label();
            this.RequestUriText = new System.Windows.Forms.Label();
            this.StatusCodeLabel = new System.Windows.Forms.Label();
            this.DurationLabel = new System.Windows.Forms.Label();
            this.StatusCodeText = new System.Windows.Forms.Label();
            this.OperationDurationText = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.StatisticsImagesCount = new System.Windows.Forms.Label();
            this.StatisticsStylesCount = new System.Windows.Forms.Label();
            this.ImagesSizeText = new System.Windows.Forms.Label();
            this.PageSpeedRankText = new System.Windows.Forms.Label();
            this.StylesSizeText = new System.Windows.Forms.Label();
            this.PageSizeText = new System.Windows.Forms.Label();
            this.ScriptsSizeText = new System.Windows.Forms.Label();
            this.FileSizeText = new System.Windows.Forms.Label();
            this.StatisticsScriptsCount = new System.Windows.Forms.Label();
            this.ImagesSizeLabel = new System.Windows.Forms.Label();
            this.PageSpeedRankLabel = new System.Windows.Forms.Label();
            this.StylesSizeLabel = new System.Windows.Forms.Label();
            this.CdnUsedText = new System.Windows.Forms.Label();
            this.PageSizeLabel = new System.Windows.Forms.Label();
            this.CdnUsedLabel = new System.Windows.Forms.Label();
            this.ScriptsSizeLabel = new System.Windows.Forms.Label();
            this.FileSizeLabel = new System.Windows.Forms.Label();
            this.StatisticsScriptsLabel = new System.Windows.Forms.Label();
            this.StatisticsStylesLabel = new System.Windows.Forms.Label();
            this.StatisticsImagesLabel = new System.Windows.Forms.Label();
            this.HelpButton = new System.Windows.Forms.Button();
            this.ImportFileButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.HostInfo = new System.Windows.Forms.GroupBox();
            this.AsNumberLabel = new System.Windows.Forms.Label();
            this.IpAddressLabel = new System.Windows.Forms.Label();
            this.TimezoneLabel = new System.Windows.Forms.Label();
            this.CityLabel = new System.Windows.Forms.Label();
            this.CountryLabel = new System.Windows.Forms.Label();
            this.TimezoneText = new System.Windows.Forms.Label();
            this.CityText = new System.Windows.Forms.Label();
            this.CountryText = new System.Windows.Forms.Label();
            this.AsNumberText = new System.Windows.Forms.Label();
            this.IpAddressText = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.HostInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // AnalyzeButton
            // 
            this.AnalyzeButton.Location = new System.Drawing.Point(283, 15);
            this.AnalyzeButton.Name = "AnalyzeButton";
            this.AnalyzeButton.Size = new System.Drawing.Size(105, 23);
            this.AnalyzeButton.TabIndex = 0;
            this.AnalyzeButton.Text = "Analyze";
            this.AnalyzeButton.UseVisualStyleBackColor = true;
            this.AnalyzeButton.Click += new System.EventHandler(this.AnalyzeButton_Click);
            // 
            // urlInput
            // 
            this.urlInput.Location = new System.Drawing.Point(81, 17);
            this.urlInput.Name = "urlInput";
            this.urlInput.Size = new System.Drawing.Size(186, 20);
            this.urlInput.TabIndex = 1;
            // 
            // DetailsListView
            // 
            this.DetailsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.type,
            this.elementUrl});
            this.DetailsListView.FullRowSelect = true;
            this.DetailsListView.GridLines = true;
            this.DetailsListView.Location = new System.Drawing.Point(519, 74);
            this.DetailsListView.Name = "DetailsListView";
            this.DetailsListView.RightToLeftLayout = true;
            this.DetailsListView.Size = new System.Drawing.Size(531, 534);
            this.DetailsListView.TabIndex = 2;
            this.DetailsListView.UseCompatibleStateImageBehavior = false;
            this.DetailsListView.View = System.Windows.Forms.View.Details;
            // 
            // type
            // 
            this.type.Text = "Element type";
            this.type.Width = 125;
            // 
            // elementUrl
            // 
            this.elementUrl.Text = "URL";
            this.elementUrl.Width = 420;
            // 
            // HistoryListView
            // 
            this.HistoryListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ItemNumber,
            this.ItemName,
            this.ItemCreationDate});
            this.HistoryListView.FullRowSelect = true;
            this.HistoryListView.GridLines = true;
            this.HistoryListView.Location = new System.Drawing.Point(283, 74);
            this.HistoryListView.MultiSelect = false;
            this.HistoryListView.Name = "HistoryListView";
            this.HistoryListView.Size = new System.Drawing.Size(230, 534);
            this.HistoryListView.TabIndex = 3;
            this.HistoryListView.UseCompatibleStateImageBehavior = false;
            this.HistoryListView.View = System.Windows.Forms.View.Details;
            this.HistoryListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.HistoryListView_ItemSelectionChanged);
            // 
            // ItemNumber
            // 
            this.ItemNumber.Text = "#";
            this.ItemNumber.Width = 35;
            // 
            // ItemName
            // 
            this.ItemName.Text = "Host";
            this.ItemName.Width = 100;
            // 
            // ItemCreationDate
            // 
            this.ItemCreationDate.Text = "Date";
            this.ItemCreationDate.Width = 100;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(9, 95);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(240, 10);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 4;
            this.progressBar.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(283, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "History";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(516, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Details";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Target URL:";
            // 
            // RequestUriLabel
            // 
            this.RequestUriLabel.AutoSize = true;
            this.RequestUriLabel.Location = new System.Drawing.Point(6, 26);
            this.RequestUriLabel.Name = "RequestUriLabel";
            this.RequestUriLabel.Size = new System.Drawing.Size(72, 13);
            this.RequestUriLabel.TabIndex = 5;
            this.RequestUriLabel.Text = "Request URI:";
            // 
            // RequestUriText
            // 
            this.RequestUriText.AutoSize = true;
            this.RequestUriText.Location = new System.Drawing.Point(122, 26);
            this.RequestUriText.Name = "RequestUriText";
            this.RequestUriText.Size = new System.Drawing.Size(16, 13);
            this.RequestUriText.TabIndex = 5;
            this.RequestUriText.Text = "...";
            // 
            // StatusCodeLabel
            // 
            this.StatusCodeLabel.AutoSize = true;
            this.StatusCodeLabel.Location = new System.Drawing.Point(6, 48);
            this.StatusCodeLabel.Name = "StatusCodeLabel";
            this.StatusCodeLabel.Size = new System.Drawing.Size(40, 13);
            this.StatusCodeLabel.TabIndex = 5;
            this.StatusCodeLabel.Text = "Status:";
            // 
            // DurationLabel
            // 
            this.DurationLabel.AutoSize = true;
            this.DurationLabel.Location = new System.Drawing.Point(6, 70);
            this.DurationLabel.Name = "DurationLabel";
            this.DurationLabel.Size = new System.Drawing.Size(50, 13);
            this.DurationLabel.TabIndex = 5;
            this.DurationLabel.Text = "Duration:";
            // 
            // StatusCodeText
            // 
            this.StatusCodeText.AutoSize = true;
            this.StatusCodeText.Location = new System.Drawing.Point(122, 48);
            this.StatusCodeText.Name = "StatusCodeText";
            this.StatusCodeText.Size = new System.Drawing.Size(16, 13);
            this.StatusCodeText.TabIndex = 5;
            this.StatusCodeText.Text = "...";
            // 
            // OperationDurationText
            // 
            this.OperationDurationText.AutoSize = true;
            this.OperationDurationText.Location = new System.Drawing.Point(122, 70);
            this.OperationDurationText.Name = "OperationDurationText";
            this.OperationDurationText.Size = new System.Drawing.Size(21, 13);
            this.OperationDurationText.TabIndex = 5;
            this.OperationDurationText.Text = "0 s";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RequestUriLabel);
            this.groupBox1.Controls.Add(this.OperationDurationText);
            this.groupBox1.Controls.Add(this.StatusCodeLabel);
            this.groupBox1.Controls.Add(this.StatusCodeText);
            this.groupBox1.Controls.Add(this.DurationLabel);
            this.groupBox1.Controls.Add(this.RequestUriText);
            this.groupBox1.Controls.Add(this.progressBar);
            this.groupBox1.Location = new System.Drawing.Point(12, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(255, 114);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Operation status";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.StatisticsImagesCount);
            this.groupBox2.Controls.Add(this.StatisticsStylesCount);
            this.groupBox2.Controls.Add(this.ImagesSizeText);
            this.groupBox2.Controls.Add(this.PageSpeedRankText);
            this.groupBox2.Controls.Add(this.StylesSizeText);
            this.groupBox2.Controls.Add(this.PageSizeText);
            this.groupBox2.Controls.Add(this.ScriptsSizeText);
            this.groupBox2.Controls.Add(this.FileSizeText);
            this.groupBox2.Controls.Add(this.StatisticsScriptsCount);
            this.groupBox2.Controls.Add(this.ImagesSizeLabel);
            this.groupBox2.Controls.Add(this.PageSpeedRankLabel);
            this.groupBox2.Controls.Add(this.StylesSizeLabel);
            this.groupBox2.Controls.Add(this.CdnUsedText);
            this.groupBox2.Controls.Add(this.PageSizeLabel);
            this.groupBox2.Controls.Add(this.CdnUsedLabel);
            this.groupBox2.Controls.Add(this.ScriptsSizeLabel);
            this.groupBox2.Controls.Add(this.FileSizeLabel);
            this.groupBox2.Controls.Add(this.StatisticsScriptsLabel);
            this.groupBox2.Controls.Add(this.StatisticsStylesLabel);
            this.groupBox2.Controls.Add(this.StatisticsImagesLabel);
            this.groupBox2.Location = new System.Drawing.Point(12, 168);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(255, 283);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Statistics";
            // 
            // StatisticsImagesCount
            // 
            this.StatisticsImagesCount.AutoSize = true;
            this.StatisticsImagesCount.Location = new System.Drawing.Point(122, 70);
            this.StatisticsImagesCount.Name = "StatisticsImagesCount";
            this.StatisticsImagesCount.Size = new System.Drawing.Size(13, 13);
            this.StatisticsImagesCount.TabIndex = 5;
            this.StatisticsImagesCount.Text = "0";
            // 
            // StatisticsStylesCount
            // 
            this.StatisticsStylesCount.AutoSize = true;
            this.StatisticsStylesCount.Location = new System.Drawing.Point(122, 48);
            this.StatisticsStylesCount.Name = "StatisticsStylesCount";
            this.StatisticsStylesCount.Size = new System.Drawing.Size(24, 13);
            this.StatisticsStylesCount.TabIndex = 5;
            this.StatisticsStylesCount.Text = "0/0";
            // 
            // ImagesSizeText
            // 
            this.ImagesSizeText.AutoSize = true;
            this.ImagesSizeText.Location = new System.Drawing.Point(122, 147);
            this.ImagesSizeText.Name = "ImagesSizeText";
            this.ImagesSizeText.Size = new System.Drawing.Size(28, 13);
            this.ImagesSizeText.TabIndex = 5;
            this.ImagesSizeText.Text = "0 kb";
            // 
            // PageSpeedRankText
            // 
            this.PageSpeedRankText.AutoSize = true;
            this.PageSpeedRankText.Location = new System.Drawing.Point(122, 227);
            this.PageSpeedRankText.Name = "PageSpeedRankText";
            this.PageSpeedRankText.Size = new System.Drawing.Size(13, 13);
            this.PageSpeedRankText.TabIndex = 5;
            this.PageSpeedRankText.Text = "0";
            // 
            // StylesSizeText
            // 
            this.StylesSizeText.AutoSize = true;
            this.StylesSizeText.Location = new System.Drawing.Point(122, 125);
            this.StylesSizeText.Name = "StylesSizeText";
            this.StylesSizeText.Size = new System.Drawing.Size(28, 13);
            this.StylesSizeText.TabIndex = 5;
            this.StylesSizeText.Text = "0 kb";
            // 
            // PageSizeText
            // 
            this.PageSizeText.AutoSize = true;
            this.PageSizeText.Location = new System.Drawing.Point(122, 205);
            this.PageSizeText.Name = "PageSizeText";
            this.PageSizeText.Size = new System.Drawing.Size(28, 13);
            this.PageSizeText.TabIndex = 5;
            this.PageSizeText.Text = "0 kb";
            // 
            // ScriptsSizeText
            // 
            this.ScriptsSizeText.AutoSize = true;
            this.ScriptsSizeText.Location = new System.Drawing.Point(122, 103);
            this.ScriptsSizeText.Name = "ScriptsSizeText";
            this.ScriptsSizeText.Size = new System.Drawing.Size(28, 13);
            this.ScriptsSizeText.TabIndex = 5;
            this.ScriptsSizeText.Text = "0 kb";
            // 
            // FileSizeText
            // 
            this.FileSizeText.AutoSize = true;
            this.FileSizeText.Location = new System.Drawing.Point(122, 183);
            this.FileSizeText.Name = "FileSizeText";
            this.FileSizeText.Size = new System.Drawing.Size(28, 13);
            this.FileSizeText.TabIndex = 5;
            this.FileSizeText.Text = "0 kb";
            // 
            // StatisticsScriptsCount
            // 
            this.StatisticsScriptsCount.AutoSize = true;
            this.StatisticsScriptsCount.Location = new System.Drawing.Point(122, 26);
            this.StatisticsScriptsCount.Name = "StatisticsScriptsCount";
            this.StatisticsScriptsCount.Size = new System.Drawing.Size(24, 13);
            this.StatisticsScriptsCount.TabIndex = 5;
            this.StatisticsScriptsCount.Text = "0/0";
            // 
            // ImagesSizeLabel
            // 
            this.ImagesSizeLabel.AutoSize = true;
            this.ImagesSizeLabel.Location = new System.Drawing.Point(6, 147);
            this.ImagesSizeLabel.Name = "ImagesSizeLabel";
            this.ImagesSizeLabel.Size = new System.Drawing.Size(65, 13);
            this.ImagesSizeLabel.TabIndex = 5;
            this.ImagesSizeLabel.Text = "Images size:";
            // 
            // PageSpeedRankLabel
            // 
            this.PageSpeedRankLabel.AutoSize = true;
            this.PageSpeedRankLabel.Location = new System.Drawing.Point(6, 227);
            this.PageSpeedRankLabel.Name = "PageSpeedRankLabel";
            this.PageSpeedRankLabel.Size = new System.Drawing.Size(90, 13);
            this.PageSpeedRankLabel.TabIndex = 5;
            this.PageSpeedRankLabel.Text = "PageSpeed rank:";
            // 
            // StylesSizeLabel
            // 
            this.StylesSizeLabel.AutoSize = true;
            this.StylesSizeLabel.Location = new System.Drawing.Point(6, 125);
            this.StylesSizeLabel.Name = "StylesSizeLabel";
            this.StylesSizeLabel.Size = new System.Drawing.Size(59, 13);
            this.StylesSizeLabel.TabIndex = 5;
            this.StylesSizeLabel.Text = "Styles size:";
            // 
            // CdnUsedText
            // 
            this.CdnUsedText.AutoSize = true;
            this.CdnUsedText.Location = new System.Drawing.Point(122, 254);
            this.CdnUsedText.Name = "CdnUsedText";
            this.CdnUsedText.Size = new System.Drawing.Size(16, 13);
            this.CdnUsedText.TabIndex = 5;
            this.CdnUsedText.Text = "...";
            // 
            // PageSizeLabel
            // 
            this.PageSizeLabel.AutoSize = true;
            this.PageSizeLabel.Location = new System.Drawing.Point(6, 205);
            this.PageSizeLabel.Name = "PageSizeLabel";
            this.PageSizeLabel.Size = new System.Drawing.Size(56, 13);
            this.PageSizeLabel.TabIndex = 5;
            this.PageSizeLabel.Text = "Page size:";
            // 
            // CdnUsedLabel
            // 
            this.CdnUsedLabel.AutoSize = true;
            this.CdnUsedLabel.Location = new System.Drawing.Point(6, 254);
            this.CdnUsedLabel.Name = "CdnUsedLabel";
            this.CdnUsedLabel.Size = new System.Drawing.Size(59, 13);
            this.CdnUsedLabel.TabIndex = 5;
            this.CdnUsedLabel.Text = "CDN used:";
            // 
            // ScriptsSizeLabel
            // 
            this.ScriptsSizeLabel.AutoSize = true;
            this.ScriptsSizeLabel.Location = new System.Drawing.Point(6, 103);
            this.ScriptsSizeLabel.Name = "ScriptsSizeLabel";
            this.ScriptsSizeLabel.Size = new System.Drawing.Size(63, 13);
            this.ScriptsSizeLabel.TabIndex = 5;
            this.ScriptsSizeLabel.Text = "Scripts size:";
            // 
            // FileSizeLabel
            // 
            this.FileSizeLabel.AutoSize = true;
            this.FileSizeLabel.Location = new System.Drawing.Point(6, 183);
            this.FileSizeLabel.Name = "FileSizeLabel";
            this.FileSizeLabel.Size = new System.Drawing.Size(103, 13);
            this.FileSizeLabel.TabIndex = 5;
            this.FileSizeLabel.Text = "File size (index.html):";
            // 
            // StatisticsScriptsLabel
            // 
            this.StatisticsScriptsLabel.AutoSize = true;
            this.StatisticsScriptsLabel.Location = new System.Drawing.Point(6, 26);
            this.StatisticsScriptsLabel.Name = "StatisticsScriptsLabel";
            this.StatisticsScriptsLabel.Size = new System.Drawing.Size(103, 13);
            this.StatisticsScriptsLabel.TabIndex = 5;
            this.StatisticsScriptsLabel.Text = "Scripts (external/all):";
            // 
            // StatisticsStylesLabel
            // 
            this.StatisticsStylesLabel.AutoSize = true;
            this.StatisticsStylesLabel.Location = new System.Drawing.Point(6, 48);
            this.StatisticsStylesLabel.Name = "StatisticsStylesLabel";
            this.StatisticsStylesLabel.Size = new System.Drawing.Size(99, 13);
            this.StatisticsStylesLabel.TabIndex = 5;
            this.StatisticsStylesLabel.Text = "Styles (external/all):";
            // 
            // StatisticsImagesLabel
            // 
            this.StatisticsImagesLabel.AutoSize = true;
            this.StatisticsImagesLabel.Location = new System.Drawing.Point(6, 70);
            this.StatisticsImagesLabel.Name = "StatisticsImagesLabel";
            this.StatisticsImagesLabel.Size = new System.Drawing.Size(44, 13);
            this.StatisticsImagesLabel.TabIndex = 5;
            this.StatisticsImagesLabel.Text = "Images:";
            // 
            // HelpButton
            // 
            this.HelpButton.Location = new System.Drawing.Point(1027, 10);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(23, 23);
            this.HelpButton.TabIndex = 8;
            this.HelpButton.Text = "?";
            this.HelpButton.UseVisualStyleBackColor = true;
            this.HelpButton.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // ImportFileButton
            // 
            this.ImportFileButton.Location = new System.Drawing.Point(438, 15);
            this.ImportFileButton.Name = "ImportFileButton";
            this.ImportFileButton.Size = new System.Drawing.Size(75, 23);
            this.ImportFileButton.TabIndex = 9;
            this.ImportFileButton.Text = "Import file";
            this.ImportFileButton.UseVisualStyleBackColor = true;
            this.ImportFileButton.Click += new System.EventHandler(this.ImportFileButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // HostInfo
            // 
            this.HostInfo.Controls.Add(this.AsNumberLabel);
            this.HostInfo.Controls.Add(this.IpAddressLabel);
            this.HostInfo.Controls.Add(this.TimezoneLabel);
            this.HostInfo.Controls.Add(this.CityLabel);
            this.HostInfo.Controls.Add(this.CountryLabel);
            this.HostInfo.Controls.Add(this.TimezoneText);
            this.HostInfo.Controls.Add(this.CityText);
            this.HostInfo.Controls.Add(this.CountryText);
            this.HostInfo.Controls.Add(this.AsNumberText);
            this.HostInfo.Controls.Add(this.IpAddressText);
            this.HostInfo.Location = new System.Drawing.Point(12, 457);
            this.HostInfo.Name = "HostInfo";
            this.HostInfo.Size = new System.Drawing.Size(255, 151);
            this.HostInfo.TabIndex = 11;
            this.HostInfo.TabStop = false;
            this.HostInfo.Text = "Host info";
            // 
            // AsNumberLabel
            // 
            this.AsNumberLabel.AutoSize = true;
            this.AsNumberLabel.Location = new System.Drawing.Point(6, 27);
            this.AsNumberLabel.Name = "AsNumberLabel";
            this.AsNumberLabel.Size = new System.Drawing.Size(62, 13);
            this.AsNumberLabel.TabIndex = 5;
            this.AsNumberLabel.Text = "AS number:";
            // 
            // IpAddressLabel
            // 
            this.IpAddressLabel.AutoSize = true;
            this.IpAddressLabel.Location = new System.Drawing.Point(6, 49);
            this.IpAddressLabel.Name = "IpAddressLabel";
            this.IpAddressLabel.Size = new System.Drawing.Size(60, 13);
            this.IpAddressLabel.TabIndex = 5;
            this.IpAddressLabel.Text = "IP address:";
            // 
            // TimezoneLabel
            // 
            this.TimezoneLabel.AutoSize = true;
            this.TimezoneLabel.Location = new System.Drawing.Point(6, 115);
            this.TimezoneLabel.Name = "TimezoneLabel";
            this.TimezoneLabel.Size = new System.Drawing.Size(56, 13);
            this.TimezoneLabel.TabIndex = 5;
            this.TimezoneLabel.Text = "Timezone:";
            // 
            // CityLabel
            // 
            this.CityLabel.AutoSize = true;
            this.CityLabel.Location = new System.Drawing.Point(6, 93);
            this.CityLabel.Name = "CityLabel";
            this.CityLabel.Size = new System.Drawing.Size(27, 13);
            this.CityLabel.TabIndex = 5;
            this.CityLabel.Text = "City:";
            // 
            // CountryLabel
            // 
            this.CountryLabel.AutoSize = true;
            this.CountryLabel.Location = new System.Drawing.Point(6, 71);
            this.CountryLabel.Name = "CountryLabel";
            this.CountryLabel.Size = new System.Drawing.Size(46, 13);
            this.CountryLabel.TabIndex = 5;
            this.CountryLabel.Text = "Country:";
            // 
            // TimezoneText
            // 
            this.TimezoneText.AutoSize = true;
            this.TimezoneText.Location = new System.Drawing.Point(122, 115);
            this.TimezoneText.Name = "TimezoneText";
            this.TimezoneText.Size = new System.Drawing.Size(16, 13);
            this.TimezoneText.TabIndex = 5;
            this.TimezoneText.Text = "...";
            // 
            // CityText
            // 
            this.CityText.AutoSize = true;
            this.CityText.Location = new System.Drawing.Point(122, 93);
            this.CityText.Name = "CityText";
            this.CityText.Size = new System.Drawing.Size(16, 13);
            this.CityText.TabIndex = 5;
            this.CityText.Text = "...";
            // 
            // CountryText
            // 
            this.CountryText.AutoSize = true;
            this.CountryText.Location = new System.Drawing.Point(122, 71);
            this.CountryText.Name = "CountryText";
            this.CountryText.Size = new System.Drawing.Size(16, 13);
            this.CountryText.TabIndex = 5;
            this.CountryText.Text = "...";
            // 
            // AsNumberText
            // 
            this.AsNumberText.AutoSize = true;
            this.AsNumberText.Location = new System.Drawing.Point(122, 27);
            this.AsNumberText.Name = "AsNumberText";
            this.AsNumberText.Size = new System.Drawing.Size(16, 13);
            this.AsNumberText.TabIndex = 5;
            this.AsNumberText.Text = "...";
            // 
            // IpAddressText
            // 
            this.IpAddressText.AutoSize = true;
            this.IpAddressText.Location = new System.Drawing.Point(122, 49);
            this.IpAddressText.Name = "IpAddressText";
            this.IpAddressText.Size = new System.Drawing.Size(16, 13);
            this.IpAddressText.TabIndex = 5;
            this.IpAddressText.Text = "...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(407, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "or";
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 620);
            this.Controls.Add(this.HostInfo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ImportFileButton);
            this.Controls.Add(this.HelpButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HistoryListView);
            this.Controls.Add(this.DetailsListView);
            this.Controls.Add(this.urlInput);
            this.Controls.Add(this.AnalyzeButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainPage";
            this.Text = "CDNChecker";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.HostInfo.ResumeLayout(false);
            this.HostInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AnalyzeButton;
        private System.Windows.Forms.TextBox urlInput;
        private System.Windows.Forms.ListView DetailsListView;
        private System.Windows.Forms.ColumnHeader type;
        private System.Windows.Forms.ColumnHeader elementUrl;
        private System.Windows.Forms.ListView HistoryListView;
        private System.Windows.Forms.ColumnHeader ItemNumber;
        private System.Windows.Forms.ColumnHeader ItemName;
        private System.Windows.Forms.ColumnHeader ItemCreationDate;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label RequestUriLabel;
        private System.Windows.Forms.Label RequestUriText;
        private System.Windows.Forms.Label StatusCodeLabel;
        private System.Windows.Forms.Label DurationLabel;
        private System.Windows.Forms.Label StatusCodeText;
        private System.Windows.Forms.Label OperationDurationText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label StatisticsImagesCount;
        private System.Windows.Forms.Label StatisticsStylesCount;
        private System.Windows.Forms.Label StatisticsScriptsCount;
        private System.Windows.Forms.Label StatisticsScriptsLabel;
        private System.Windows.Forms.Label StatisticsStylesLabel;
        private System.Windows.Forms.Label StatisticsImagesLabel;
        private System.Windows.Forms.Label FileSizeText;
        private System.Windows.Forms.Label FileSizeLabel;
        private System.Windows.Forms.Label PageSizeText;
        private System.Windows.Forms.Label PageSizeLabel;
        private System.Windows.Forms.Label PageSpeedRankText;
        private System.Windows.Forms.Label PageSpeedRankLabel;
        private System.Windows.Forms.Button HelpButton;
        private System.Windows.Forms.Label ImagesSizeText;
        private System.Windows.Forms.Label StylesSizeText;
        private System.Windows.Forms.Label ScriptsSizeText;
        private System.Windows.Forms.Label ImagesSizeLabel;
        private System.Windows.Forms.Label StylesSizeLabel;
        private System.Windows.Forms.Label ScriptsSizeLabel;
        private System.Windows.Forms.Label CdnUsedLabel;
        private System.Windows.Forms.Label CdnUsedText;
        private System.Windows.Forms.Button ImportFileButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox HostInfo;
        private System.Windows.Forms.Label AsNumberLabel;
        private System.Windows.Forms.Label IpAddressLabel;
        private System.Windows.Forms.Label TimezoneLabel;
        private System.Windows.Forms.Label CityLabel;
        private System.Windows.Forms.Label CountryLabel;
        private System.Windows.Forms.Label TimezoneText;
        private System.Windows.Forms.Label CityText;
        private System.Windows.Forms.Label CountryText;
        private System.Windows.Forms.Label AsNumberText;
        private System.Windows.Forms.Label IpAddressText;
        private System.Windows.Forms.Label label4;
    }
}

