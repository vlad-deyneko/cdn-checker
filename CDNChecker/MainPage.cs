﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using CDNChecker.Models;
using HtmlAgilityPack;
using Newtonsoft.Json;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
using Image = CDNChecker.Models.Image;

namespace CDNChecker
{
    public partial class MainPage : Form
    {
        private int _cdnResourcesCount;

        private readonly string[] _cdnList =
        {
            "cdn",
            "akamai.net",
            "akamaized.net",
            "akamaiedge.net",
            "akamaihd.net",
            "akamaitechnologies.com",
            "akamaitechnologies.fr",
            "azureedge.net",
            "cloudfront.net",
            "cloudflare.com",
            "facebook.com",
            "facebook.net",
            "fbcdn.net",
            "fastly.net",
            "edgesuite.net",
            "edgekey.net",
            "google",
            "googlesyndication",
            "doubleclick.net",
            "googlehosted",
            "gstatic",
            "youtube",
            "llnwd.net",
            "srip.net",
            "twimg.com",
            "yimg",
            "yahooapis",
            "wp.com"
        };


        public MainPage()
        {
            InitializeComponent();
            LoadHistory();
        }

        private async void AnalyzeButton_Click(object sender, EventArgs e)
        {
            progressBar.Visible = true;
            string targetUrl = urlInput.Text;

            if (!string.IsNullOrWhiteSpace(targetUrl))
            {
                DetailsListView.Items.Clear();
                ClearStatistics();
                
                try
                {
                    await SetHostInfo(targetUrl);
                    await AnalyzeUrl(targetUrl);
                    await SetPageRankStatistics(targetUrl);
                }
                catch
                {
                    MessageBox.Show(
                        "Whoops, something went wrong! Looks like I can't connect to that hostname. Maybe this address is invalid?",
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(
                    "Address field cannot be empty! You must enter a resource URL in order to continue.",
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            progressBar.Visible = false;
        }

        private void ClearCdnResult()
        {
            _cdnResourcesCount = 0;
        }

        private void SetCdnResult()
        {
            if (_cdnResourcesCount > 0)
            {
                CdnUsedLabel.ForeColor = Color.Teal;
                CdnUsedText.ForeColor = Color.Teal;
                CdnUsedText.Text = "true";
            }
            else
            {
                CdnUsedLabel.ForeColor = Color.Crimson;
                CdnUsedText.ForeColor = Color.Crimson;
                CdnUsedText.Text = "false";
            }
        }

        /// <summary>
        /// Analyses web document and adds statistics to the History.
        /// </summary>
        /// <param name="document">Document to analyze.</param>
        private async Task AnalyzeWebDocument(DocumentWithLinks document)
        {
            ClearCdnResult();
            // Details list
            // Scripts
            ListViewGroup scriptingGroup = new ListViewGroup
            {
                Name = "Scripts",
                Header = $"Referenced scripts ({document.Scripts.Count} items):"
            };
            DetailsListView.Groups.Add(scriptingGroup);
            foreach (Script s in document.Scripts)
            {
                ListViewItem item = new ListViewItem
                {
                    Text = s.Type,
                    Group = scriptingGroup
                };
                item.SubItems.Add(s.Src + "\r\n");
                foreach (string cdn in _cdnList)
                {
                    if (s.Src.Contains(cdn) && !string.IsNullOrWhiteSpace(s.Src))
                    {
                        _cdnResourcesCount += 1;
                    }
                }
                DetailsListView.Items.Add(item);
            }

            // Styles
            ListViewGroup stylesGroup = new ListViewGroup
            {
                Name = "Styles",
                Header = $"Referenced styles ({document.Styles.Count} items):"
            };

            DetailsListView.Groups.Add(stylesGroup);
            foreach (Style s in document.Styles)
            {
                ListViewItem item = new ListViewItem
                {
                    Text = s.Rel,
                    Group = stylesGroup
                };
                item.SubItems.Add(s.Href + "\r\n");
                foreach (string cdn in _cdnList)
                {
                    if (s.Rel.Contains(cdn) && !string.IsNullOrWhiteSpace(s.Rel))
                    {
                        _cdnResourcesCount += 1;
                    }
                }
                DetailsListView.Items.Add(item);
            }

            // Images
            ListViewGroup imagesGroup = new ListViewGroup
            {
                Name = "Images",
                Header = $"Referenced images ({document.Images.Count} items):"
            };

            DetailsListView.Groups.Add(imagesGroup);
            foreach (Image i in document.Images)
            {
                ListViewItem item = new ListViewItem
                {
                    Text = i.Alt,
                    Group = imagesGroup
                };
                item.SubItems.Add(i.Src + "\r\n");
                foreach (string cdn in _cdnList)
                {
                    if (i.Src.Contains(cdn) && !string.IsNullOrWhiteSpace(i.Src))
                    {
                        _cdnResourcesCount += 1;
                    }
                }
                DetailsListView.Items.Add(item);
            }

            // URLs
            ListViewGroup referencesGroup = new ListViewGroup
            {
                Name = "URLs",
                Header = $"Referenced URLs ({document.References.Count} items):"
            };

            DetailsListView.Groups.Add(referencesGroup);
            foreach (Url u in document.References)
            {
                ListViewItem item = new ListViewItem
                {
                    Text = "url",
                    Group = referencesGroup
                };
                item.SubItems.Add(u.Href + "\r\n");
                DetailsListView.Items.Add(item);
            }

            StatisticsScriptsCount.Text = document.Scripts.Count.ToString();
            StatisticsStylesCount.Text = document.Styles.Count.ToString();
            StatisticsImagesCount.Text = document.Images.Count.ToString();
            SetCdnResult();
        }

        private enum SourceType
        {
            Url,
            HtmlFile
        }

        /// <summary>
        /// Returns parsed document for specified source (URL or HTML file).
        /// </summary>
        /// <param name="type">Source type: URL or filename.</param>
        /// <param name="targetUrl">Hostname URL (example.com)</param>
        /// <param name="filePath">Full absolute file path.</param>
        /// <returns></returns>
        private DocumentWithLinks GetParsedDocument(SourceType type, string targetUrl = "", string filePath = "")
        {
            HtmlWeb hw = new HtmlWeb();

            HtmlDocument document = new HtmlDocument();
            if (type == SourceType.Url)
            {
                string url = $"http://{targetUrl}";
                document = hw.Load(url);
                SaveToFile(document, targetUrl);

                RequestUriText.Text = hw.ResponseUri.AbsoluteUri;
                StatusCodeText.Text = hw.StatusCode.ToString();
                OperationDurationText.Text = $"{hw.RequestDuration / 1000.0} s.";
            }
            else
            {
                RequestUriText.Text = "...";
                StatusCodeText.Text = "...";
                OperationDurationText.Text = "0 s";
                document.Load(filePath);
            }
            return new DocumentWithLinks(document);
        }

        /// <summary>
        /// Returns parsed document for specified source (URL or HTML file).
        /// </summary>
        /// <param name="targetUrl">Hostname URL (example.com)</param>
        private DocumentWithLinks GetParsedDocument(string targetUrl)
        {
            HtmlWeb hw = new HtmlWeb();
            HtmlDocument document;
            
            string url = $"http://{targetUrl}";
            try
            {
                document = hw.Load(url);
                SaveToFile(document, targetUrl);
            }
            catch
            {
                return new DocumentWithLinks(null);
            }
            
            return new DocumentWithLinks(document);
        }

        private async Task AnalyzeHtmlFile(string fileName)
        {
            string filePath = GetHtmlFilePath(fileName);
            DocumentWithLinks nwl = GetParsedDocument(SourceType.HtmlFile, filePath: filePath);

            await AnalyzeWebDocument(nwl);

            FileInfo htmlFile = new FileInfo(GetHtmlFilePath(fileName));
            FileSizeText.Text = $"{htmlFile.Length / 1024.0:N2} kb";
        }

        private async Task AnalyzeUrl(string targetUrl)
        {
            // Web pages list
            ListViewItem hostItem = new ListViewItem
            {
                Text = (HistoryListView.Items.Count + 1).ToString()
            };
            hostItem.SubItems.Add(targetUrl);
            hostItem.SubItems.Add($"{DateTime.Now:dd.MM.yyyy HH:mm}");
            HistoryListView.Items.Add(hostItem);

            DocumentWithLinks nwl = GetParsedDocument(SourceType.Url, targetUrl);
            await AnalyzeWebDocument(nwl);

            FileInfo htmlFile = new FileInfo(GetHtmlFilePath(targetUrl));
            FileSizeText.Text = $"{htmlFile.Length / 1024.0:N2} kb";
        }

        /// <summary>
        /// Sets all statistic labels to their default values.
        /// </summary>
        private void ClearStatistics()
        {
            RequestUriText.Text = "...";
            StatusCodeText.Text = "...";
            OperationDurationText.Text = "0 s";

            StatisticsImagesCount.Text = "0/0";
            StatisticsStylesCount.Text = "0/0";
            StatisticsScriptsCount.Text = "0/0";

            StylesSizeText.Text = "0 kb";
            ScriptsSizeText.Text = "0 kb";
            ImagesSizeText.Text = "0 kb";

            PageSizeText.Text = "0 kb";
            PageSpeedRankText.Text = "0";
            PageSpeedRankLabel.ForeColor = Color.Black;
            PageSpeedRankText.ForeColor = Color.Black;

            CdnUsedText.Text = "...";

            FileSizeText.Text = "0kb";
        }

        /// <summary>
        /// Gets page statistics and SpeedRank based on Google API.
        /// </summary>
        /// <param name="hostName">Target hostname (example.com).</param>
        /// <returns></returns>
        private async Task<PageSpeedReport> GetPageSpeedRank(string hostName)
        {
            string pageSpeedUri =
                $"https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=http://{hostName}&filter_third_party_resources=false&screenshot=false&strategy=desktop&fields=id%2CinvalidRules%2CpageStats%2CresponseCode%2CruleGroups%2Ctitle&key=AIzaSyB3wzmgOYbAUWf2tsnCBd2enFFvqr_6tQg";
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(new Uri(pageSpeedUri));
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<PageSpeedReport>(responseBody);
            }
        }

        private async Task SetPageRankStatistics(string targetUrl)
        {
            PageSpeedReport pageData = await GetPageSpeedRank(targetUrl);
            PageStatistics pageStats = pageData.PageStats;
            double totalPageSize =
                double.Parse(pageStats.HtmlResponseBytes)
                + double.Parse(pageStats.CssResponseBytes)
                + double.Parse(pageStats.JavascriptResponseBytes)
                + double.Parse(pageStats.ImageResponseBytes)
                + double.Parse(pageStats.OtherResponseBytes);
            StatisticsStylesCount.Text = $"{StatisticsStylesCount.Text}/{pageStats.NumberCssResources}";
            StatisticsScriptsCount.Text = $"{StatisticsScriptsCount.Text}/{pageStats.NumberJsResources}";

            ScriptsSizeText.Text = $"{double.Parse(pageStats.JavascriptResponseBytes) / 1024:N2} kb";
            StylesSizeText.Text = $"{double.Parse(pageStats.CssResponseBytes) / 1024:N2} kb";
            ImagesSizeText.Text = $"{double.Parse(pageStats.ImageResponseBytes) / 1024:N2} kb";

            int pageScore = pageData.RuleGroups.Speed.Score;
            if (pageScore < 50)
            {
                PageSpeedRankLabel.ForeColor = Color.Crimson;
                PageSpeedRankText.ForeColor = Color.Crimson;
            }
            else if (pageScore > 50 && pageScore < 80)
            {
                PageSpeedRankLabel.ForeColor = Color.OrangeRed;
                PageSpeedRankText.ForeColor = Color.OrangeRed;
            }
            else
            {
                PageSpeedRankLabel.ForeColor = Color.Teal;
                PageSpeedRankText.ForeColor = Color.Teal;
            }

            PageSpeedRankText.Text = pageScore.ToString();
            PageSizeText.Text = $"{totalPageSize / 1024:N2} kb";
        }

        private async Task SetHostInfo(string hostName)
        {
            HostInfo hostInfo = await GetHostInfo(hostName);
            if (hostInfo.Status == "success")
            {
                AsNumberText.Text = hostInfo.As;
                IpAddressText.Text = hostInfo.Query;
                CountryText.Text = hostInfo.Country;
                CityText.Text = hostInfo.City;
                TimezoneText.Text = hostInfo.Timezone;
            }
        }

        private static async Task<HostInfo> GetHostInfo(string hostName)
        {
            string pageSpeedUri = $"http://ip-api.com/json/{hostName}";
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(new Uri(pageSpeedUri));
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<HostInfo>(responseBody);
            }
        }

        /// <summary>
        /// Adds analyzed and saved pages to HistoryListView.
        /// </summary>
        private void LoadHistory()
        {
            List<string> allFiles = GetAllFiles();
            foreach (string fileName in allFiles)
            {
                ListViewItem item = new ListViewItem
                {
                    Text = (HistoryListView.Items.Count + 1).ToString()
                };
                FileInfo htmlFile = new FileInfo(GetHtmlFilePath(fileName));
                item.SubItems.Add(fileName);
                item.SubItems.Add($"{htmlFile.CreationTime:dd.MM.yyyy HH:mm}");
                HistoryListView.Items.Add(item);
            }
        }

        /// <summary>
        /// Returns Documents folder path for currently logged user.
        /// </summary>
        private static string GetDocumentsFolderPath()
        {
            string path = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
            if (Environment.OSVersion.Version.Major >= 6)
            {
                path = Directory.GetParent(path).ToString();
            }
            path = path + @"\Documents\";
            return path;
        }

        /// <summary>
        /// Return list of HTML files saved by application earlier.
        /// </summary>
        /// <returns>Array of HTML filenames.</returns>
        private static List<string> GetAllFiles()
        {
            string appFolder = GetDocumentsFolderPath() + "CDNChecker";
            
            IEnumerable<string> allFiles = Directory.EnumerateFiles(appFolder, "*.html", SearchOption.TopDirectoryOnly);
            List<string> fileNames = new List<string>();
            foreach (var fileName in allFiles)
            {
                fileNames.Add(fileName.Substring(appFolder.Length + 1, fileName.Length - appFolder.Length - 6));
            }
            return fileNames;
        }

        /// <summary>
        /// Returns full HTML file path for specified file name.
        /// </summary>
        /// <param name="fileName"></param>
        private static string GetHtmlFilePath(string fileName)
        {
            return GetDocumentsFolderPath() + $"CDNChecker/{fileName}.html";
        }

        /// <summary>
        /// Saves specified HTML document to the file in user Documents folder.
        /// </summary>
        /// <param name="document">HTML document to save.</param>
        /// <param name="fileName">Desired file name.</param>
        private static void SaveToFile(HtmlDocument document, string fileName)
        {
            string documentsFolder = GetDocumentsFolderPath();
            if (!Directory.Exists(documentsFolder + "CDNChecker"))
            {
                Directory.CreateDirectory(documentsFolder + "CDNChecker");
            }
            document.Save(documentsFolder + $"CDNChecker/{fileName}.html");
        }

        private async void HistoryListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            progressBar.Visible = true;
            ClearStatistics();
            DetailsListView.Items.Clear();
            await AnalyzeHtmlFile(e.Item.SubItems[1].Text);
            progressBar.Visible = false;
        }

        private void HelpButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "This small utility helps to identify if a web page is using external resources.\n\nExternal resources can be downloaded in parallel to improve page download speed and reduce server load.\n\nTo start working, type resource address in the address field and click 'Analyze' button.",
                @"Information",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        /// <summary>
        /// Returns an array of hosts in the selected file. Hosts must be separated by new line.
        /// </summary>
        /// <param name="filePath"></param>
        private static string[] GetHostsFromFile(string filePath)
        {
            string[] hostsFile = File.ReadAllLines(filePath);
            return hostsFile;
        }

        private async void ImportFileButton_Click(object sender, EventArgs e)
        {
            progressBar.Visible = true;
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string fileName = openFileDialog1.FileName;
                string[] hostsList = GetHostsFromFile(fileName);
                await Task.Run(() => GenerateReport(hostsList));
            }
            progressBar.Visible = false;
        }

        /// <summary>
        /// Generates report for the list of hosts. Saves the report to the CSV file in Documents folder.
        /// </summary>
        /// <param name="hostsList"></param>
        private void GenerateReport(string[] hostsList)
        {
            List<string> report = new List<string>();
            foreach (string host in hostsList)
            {
                report.Add(AnalyzeHost(host).Result);
            }
            string documentsFolder = GetDocumentsFolderPath();

            string time = $"{DateTime.Now.ToLocalTime():dd.MM.yyyy-HH.mm}";
            File.WriteAllLines(documentsFolder + $"CDNChecker/Report-{hostsList.Length}-hosts-{time}.csv", report);
        }

        /// <summary>
        /// Returns report result in CSV formatted string.
        /// </summary>
        /// <param name="hostName"></param>
        private async Task<string> AnalyzeHost(string hostName)
        {
            HostInfo hostInfo = await GetHostInfo(hostName);
            string hostCountry = "";
            if (hostInfo.Status == "success")
            {
                hostCountry = hostInfo.Country;
            }
            int cdnResourceCount = 0;
            string result;
            DocumentWithLinks nwl = GetParsedDocument(hostName);
            if (nwl.Scripts != null)
            {
                foreach (Script script in nwl.Scripts)
                {
                    foreach (string cdn in _cdnList)
                    {
                        if (script.Src.Contains(cdn))
                        {
                            cdnResourceCount += 1;
                        }
                    }
                }
                foreach (Style style in nwl.Styles)
                {
                    foreach (string cdn in _cdnList)
                    {
                        if (style.Rel.Contains(cdn))
                        {
                            cdnResourceCount += 1;
                        }
                    }
                }
                foreach (Image image in nwl.Images)
                {
                    foreach (string cdn in _cdnList)
                    {
                        if (image.Src.Contains(cdn))
                        {
                            cdnResourceCount += 1;
                        }
                    }
                }
                result = $"{hostName},{nwl.Scripts.Count},{nwl.Styles.Count},{nwl.Images.Count},{cdnResourceCount > 0},{cdnResourceCount},{hostCountry}";
            }
            else
            {
                result = $"{hostName},{0},{0},{0},{false},{0},{hostCountry}";
            }
            return result;
        }
    }
}
